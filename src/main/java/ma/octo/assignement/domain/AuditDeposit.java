package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.util.EventType;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_D")
@Getter
@Setter
@DynamicUpdate
public class AuditDeposit {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

}
